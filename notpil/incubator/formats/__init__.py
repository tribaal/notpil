from notpil.incubator.formats.gif import GIF
from notpil.incubator.formats.png import PNG

INCUBATOR_FORMATS = {
    'gif': GIF,
    'png': PNG,
}
