######
notpil
######

An alternative for the unmaintained PIL.


***********
Development
***********

Pixels are stored as a list of arrays (``array.array``). Actual pixels can be
retrieved by reading ``Image.pixelsize`` ints from the array.
